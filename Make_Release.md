# How to make a release
1. Update Makefile.am for the files to be released
2. Update release version in configure.ac
3. ./autogen.sh ; ./configure; make dist
4. Create a release in Topomatch -> deployment -> release
5. Create a token (MY-PRIVATE-TOKEN) in *EJ Picture* -> edit_profile -> "Access Tokens" with "api" as scope
6. Upload file (Topomatch is project ID 15442):
   curl --request POST --header "PRIVATE-TOKEN:*MY-PRIVATE-TOKEN*" --form "file=@*file-generated-in-step-3*" "https://gitlab.inria.fr/api/v4/projects/15442/uploads"
7. Update README.md with new link
8. git commit ; git push
9. Update link of release in Topomatch -> deployment -> release
