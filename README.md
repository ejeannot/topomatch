# TopoMatch

## Process mapping algorithms and tools for general topologies

Topomatch is an extension of the <a href="https://gforge.inria.fr/frs/?group_id=2906" target="_blank">TreeMatch library</a>.

TopoMatch leverages on the Scotch library to handle any type of topologies and not only trees. 

Its main features are: 
* Handling any type of topologies (tgt Scotch format or hwloc format).
* Handle large communication patterns (up to hundreds of thousands of processes and processing units) .
* Manage binding constraints: you can specify a subset of the node onto which you want to do the mapping.
* Manage oversubscribing: you can specify that more than one processes can be mapped onto a each processing unit.
* Deal with logical numbering. Physical core numbering can be used with XML/HWLOC topologies. 
* Provide exhaustive search for small cases.
* Adaptive algorithmic that provide a good trade-off between quality and speed.
* Crucial sections of the code are multithreaded.
* Optimize I/O to read large input files.
* Portable on Unix-like systems (Linux, OS-X, etc.). 
* Many useful options (level of verbosity, topology optimization, partitioning, etc.).

## Download package
[Version 1.3.1 (November 2024)](https://gitlab.inria.fr/-/project/15442/uploads/8d197c8f925352c5355e79ee9d5488b1/topomatch-1.3.1.tar.gz)

## Getting started
TopoMatch have been successfully compiled on MacOSX (Clang) and Linux (gcc).

It depends on <a href="https://www.open-mpi.org/projects/hwloc/"
target="_blank">HWLOC</a> and optionaly on
<a href="https://gforge.inria.fr/projects/scotch" target="_blank">Scotch V6.0.7 or later</a>

* create configure: ./autogen.sh (optional: required only if configure is not available)
* configure the makefiles: ./configure

or

* ./configure SCOTCH_DIR=/dir/to/Scotch/install (if you need to specify the Scotch directory: where the "lib" or "lib64" dir is once Scotch is built)
* complie: make
* test: make check
* install: make install (or sudo make install depending on where you are installing TopoMatch)

More details in README and in INSTALL files.

## View documentation
[Documentation](https://gitlab.inria.fr/ejeannot/topomatch/blob/master/doc/topomatch_doc.txt).


## Contact 
In case of problems, please contact  [Emmanuel Jeannot](mailto:emmanuel.jeannot@inria.fr).
