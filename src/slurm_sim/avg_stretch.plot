set term pdf dashed enhanced color font ",9"
set output "avg_stretch.pdf" 

set title 'Average Stretch'
set xlabel 'Percentage of Communication of the Application Runtime' 
show xlabel
set ylabel 'Ratio to SLURM'
show ylabel

set key bottom right
#set xtics 0 20000
#set yrange [-100:1200]

plot "avg_stretch.res" using  ($1*100):($2/$3)   with linespoint lw 3 title "SLURM then TM" ,\
     "avg_stretch.res" using  ($1*100):($2/$4)   with linespoint lw 3 title "SLURM and TM" 