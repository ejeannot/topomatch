#!/usr/bin/perl -w

#run as: ./generate_res.pl /home/ejeannot/recherche/src/topomatch/src/slurm_sim/riplay/random_matrices_res 0.3 1,2,3,4,5,10 "Avg flow" > avg_flow.res

$prefix    = $ARGV[0];
$c         = $ARGV[1];
@durations = split /,/,$ARGV[2];
$metric    = $ARGV[3];

foreach $d (@durations) {
   print "$d\t";
   foreach $s (0,1,2) {
      $cc = $c;
      $cc = "0.1" if ($s == 0);
      $dir = "$prefix/$d";
      $filename = "$dir/simulation_".$s."_".$cc.".txt";
      @lines = `./parse_res.pl $filename`;
      foreach $line (@lines) {
	 if (($value)=($line =~ /$metric: (\S+)/)) {
	    print "$value\t";
	 }
      }
   }
   print "\n";
}
