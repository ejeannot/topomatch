#!/usr/bin/perl -w

#run as: ./generate_res.pl /home/ejeannot/recherche/src/topomatch/src/slurm_sim/riplay/random_matrices_res 1 "Avg flow" > avg_flow.res

$prefix = $ARGV[0];
$hour   = $ARGV[1];
$metric = $ARGV[2];

$dir = "$prefix/$hour";

foreach $c (0.1,0.3,0.5) {
   print "$c\t";
   foreach $s (0,1,2) {
      $cc = $c;
      $cc = "0.1" if ($s == 0);
      $filename = "$dir/simulation_".$s."_".$cc.".txt";
      @lines = `./parse_res.pl $filename`;
      foreach $line (@lines) {
	 if (($value)=($line =~ /$metric: (\S+)/)) {
	    print "$value\t";
	 }
      }
   }
   print "\n";
}
