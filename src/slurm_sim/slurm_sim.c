#include "tm_malloc.h"
#include "tm_mapping.h"
#include <ctype.h>
#include <stdio.h>
#include "tm_timings.h"

/* for debugging malloc */
/* #define __DEBUG_TM_MALLOC__*/
#undef __DEBUG_TM_MALLOC__
#ifdef __DBUG_TM_MALLOC__
#include "tm_malloc.h"
#define MALLOC(x) tm_malloc(x,__FILE__,__LINE__)
#define CALLOC(x,y) tm_calloc(x,y,__FILE__,__LINE__)
#define REALLOC(x,y) tm_realloc(x,y,__FILE__,__LINE__)
#define FREE   tm_free
#define MEM_CHECK tm_mem_check
#else
#define MALLOC    malloc
#define CALLOC    calloc
#define FREE      free
#define REALLOC   realloc
#define MEM_CHECK tm_mem_check
#endif

#define SLURM_OVERHEAD 2
#define TM_FACTOR 1
typedef enum  {
    RND_MAT  = 1,
    EXTRAPOLATED_MAT = 2,
} matrix_type_t;

/* #define COM_MAT_DIR "/home/ejeannot/topomatch/src/slurm_sim/com_mat" */
#define COM_MAT_DIR "/Users/ejeannot/recherche/src/topomatch/trunk/src/slurm_sim/com_mat"
/* #define COM_MAT_DIR "/home/jeannot/slurm_sim/com_mat" */

/**/
int check_constraints(tm_topology_t  *topology, int **constraints);
tree_t *kpartition_build_tree_from_topology(tm_topology_t *topology,double **comm,int N, int *constraints, int nb_constraints, double *obj_weight, double *com_speed);

int simulation_type=0;
double communication_ratio=1.0;
matrix_type_t matrix_type = RND_MAT;
int subtree_level_shift = 0;
int  max_subtree_shift = 0;

#define LINE_SIZE 10240
#define N 18
enum event_type_t {SUBMIT_JOB, START_JOB, END_JOB};

typedef struct _job_t{
  int id;
  int nb_procs;
  int *allocated_procs;
  int submit_date;
  int schedule_date;
  int start_date;
  int end_date;
  int duration;
}job_t;

typedef struct _job_queue_t{
  job_t *job;
  struct _job_queue_t *next;
  struct _job_queue_t *last;
}job_queue_t;


typedef struct _event_list_t{
  enum event_type_t type;
  int date;
  job_t *job;
  struct _event_list_t *next;
}event_list_t;


int upscale_mat(affinity_mat_t *aff_mat, int n1, int n2){
  double **res;
  double **mat = aff_mat->mat;
  double r;
  int i,j,i1,i2,j1,j2;
  res = (double **)MALLOC(sizeof(double *)*n2);
  for (i=0; i<n2; i++)
    res[i] = (double *)MALLOC(sizeof(double)*n2);

  /* for(i=0;i<n1;i++){ */
  /*   for(j=0;j<n1;j++){ */
  /*     printf("%.2f ",mat[i][j]); */
  /*   } */
  /*   printf("\n"); */
  /* } */

  /* printf("\n"); */


  r = n2/(n1*1.0);
  /* printf("r=%.2f\n",r); */
  // Bilinear interpomlation
  for(i=0;i<n2;i++){
    i1 = (i*n1)/n2;
    i2 = (i*n1)%n2?(i1+1):i1;
    if(i2>=n1)
      i2=n1-1;
    for(j=0;j<n2;j++){
      j1 = (j*n1)/n2;
      j2 = (j*n1)%n2?(j1+1):j1;
      if(j2>=n1)
	j2=n1-1;
      /* printf("i=%d, j=%d, i1=%d, i2=%d, j1=%d, j2=%d\n",i,j,i1,i2,j1,j2); */
      if(j1 == j2){
	if(i1 == i2){
	  res[i][j] = mat[i1][j1];
	}else{
	  res[i][j] = ((i2*r-i)/(r*(i2-i1)))*mat[i1][j1]+((i-i1*r)/(r*(i2-i1)))*mat[i2][j1];
	}
      }else if(i1 == i2){
	res[i][j] = ((j2*r-j)/(r*(j2-j1)))*mat[i1][j1]+((j-j1*r)/(r*(j2-j1)))*mat[i1][j2];
      }else{
	res[i][j] = (1.0/(r*r*(i1-i2)*(j1-j2)))*
	  (mat[i1][j1]* (i2*r-i) * (j2*r-j)+
	   mat[i2][j1]* (i-i1*r) * (j2*r-j)+
	   mat[i1][j2]* (i2*r-i) * (j-j1*r)+
	   mat[i2][j2]* (i-i1*r) * (j-j1*r));
      }
      /* printf ("%.2f ",res[i][j]); */
    }
    /* printf("\n"); */
  }
  for (i=0; i<n1; i++)
    FREE(mat[i]);
  FREE(mat);


  aff_mat->mat =res;
  aff_mat -> order = n2;
  /* exit(-1); */
  return n2;
}

void FREE_job(job_t *job){
  FREE(job->allocated_procs);
  FREE(job);
}


job_t *create_job(int id, int nb_procs, int submit_date, int duration){
  job_t *job = CALLOC(1,sizeof(job_t));

  job->id               = id;
  job->nb_procs         = nb_procs;
  job->submit_date      = submit_date;
  job->duration         = duration;
  job->allocated_procs = CALLOC(1,sizeof(int)*nb_procs);
  return job;
}

int global_date = 0;
int nb_available_procs = 8;

void display_job(job_t *job){
  printf("id: %d nb_procs: %d sub: %d sched: %d start: %d end: %d duration: %d\n",
	 job->id, job->nb_procs, job->submit_date,
	 job->schedule_date, job->start_date, job->end_date,
	 job->duration);

}

void display_event(event_list_t *event){
  printf("Type : %d, date : %d, job: ", event->type, event->date); display_job(event->job);
}

tm_topology_t *build_tm_topology(int subtree_i, tm_topology_t *slurm_topology){
  int i;
  tm_topology_t *topology = NULL;

  topology            = (tm_topology_t*)MALLOC(sizeof(tm_topology_t));
  topology->nb_levels = slurm_topology->nb_levels - subtree_i;
  topology->arity     = (int*)MALLOC(sizeof(int)*topology->nb_levels);
  topology->cost      = (double*)MALLOC(sizeof(double)*topology->nb_levels);

  for( i = 0 ; i < topology->nb_levels-1 ; i++ ){
    topology->arity[i] = slurm_topology->arity[subtree_i+i];
    topology->cost[i] = slurm_topology->cost[subtree_i+i];
  }

  topology->arity[topology->nb_levels-1] = 0;
  topology->cost[topology->nb_levels-1] = slurm_topology->cost[topology->nb_levels-1];

  build_synthetic_proc_id(topology);

  return topology;
}


int loc_in_tab(int *tab, int n, int shift, int val){
  int i;
  for( i = 0; i < n ; i++)
    if(tab[i] - shift  == val){
      return 1;
    }
    return 0;
}

void set_constraints_from_job(tm_topology_t *topology, job_t *job, int shift){
  int *constraints = NULL;
  int nb_constraints = job->nb_procs;
  int i;

  constraints = (int*) MALLOC(sizeof(int)*nb_constraints);

  for(i=0 ; i<nb_constraints ; i++)
    constraints[i] = job->allocated_procs[i] -shift;
  topology -> constraints = constraints;
  topology -> nb_constraints = nb_constraints;
}


void set_constraints_from_topology(tm_topology_t *topology, int shift, int subtree, int sts, tm_topology_t *slurm_topology, int **rr_sol, int nb_processes){
  int *constraints = NULL;
  int j,index;
  int nb_constraints = 0;
  int last_level = slurm_topology->nb_levels-1;

  /* Topomatch cannot use nodes that are not free according to SLURM
   and build teh Round Robin solution at the same time*/
  *rr_sol = (int*)MALLOC(sizeof(int)*nb_processes);
  index = 0;
  for (j = sts*subtree ; j < sts*(subtree+1) ; j++){
    if (slurm_topology->free_nodes[last_level][j]){
      nb_constraints ++;
      if(index < nb_processes){
	(*rr_sol)[index] = j-shift;
	index ++;
      }
    }
  }

  index = 0;
  constraints = (int*) MALLOC(sizeof(int)*nb_constraints);
  for (j = sts*subtree ; j < sts*(subtree+1) ; j++){
    if (slurm_topology->free_nodes[last_level][j]){
      constraints[index] = topology->node_id[topology->nb_levels - 1][j-shift];
      index++;
    }
  }


  topology -> constraints = constraints;
  topology -> nb_constraints = nb_constraints;
}

int loc_distance(tm_topology_t *topology,int i, int j)
{
  int level = 0;
  int arity=-1;
  int f_i, f_j ;
  int depth = topology->nb_levels-1;

  f_i = topology->node_rank[depth][i];
  f_j = topology->node_rank[depth][j];

  f_i = i;
  f_j = j;

  do{
    level++;
    arity = topology->arity[level];
    if( arity == 0 )
      arity = 1;
    f_i = f_i/arity;
    f_j = f_j/arity;
    /* printf("%d: %d-%d -> %d\n",arity, f_i, f_j,level); */
 } while((f_i!=f_j) && (level < depth));


  return level;
}

double loc_eval_sol(int n,int *Value, affinity_mat_t *aff_mat,  tm_topology_t *topology)
{
  double a,c,sol;
  int i,j;
  double *cost = topology->cost;
  double **mat = aff_mat->mat;

  sol = 0;
  for ( i = 0 ; i < n ; i++ )
    for ( j = i+1 ; j < n ; j++){
      c = mat[i][j];
      a = cost[distance(topology,Value[i],Value[j])];
      a = loc_distance(topology,Value[i],Value[j]);
      /* printf("T_%d_%d -> (%d,%d) %f/%f=%f\n",i,j,Value[i], Value[j], c,a,c/a); */
      sol += c/a;
    }

  printf("%g\n",sol);

  return sol;
}



double loc_eval_sol_inv(int n,int *Value, affinity_mat_t *aff_mat,  tm_topology_t *topology)
{
  double a,c,sol;
  int i,j;
  double *cost = topology->cost;
  double **mat = aff_mat->mat;

  sol = 0;
  for ( i = 0 ; i < n ; i++)
    for ( j = i+1 ; j < n ; j++){
      c = mat[i][j];
      a = cost[distance(topology,Value[i],Value[j])];
      a = loc_distance(topology,Value[i],Value[j]);
      /* printf ("%d<->%d = %d\n",Value[i],Value[j],(int)a); */
      sol += c*a;
    }

  printf("%g\n",sol);

  return sol;
}




void update_job_runtime(job_t *job, int subtree_i,  int shift,
			tm_topology_t *slurm_topology){

  char            *sparse[3]={"0.1","0.5","1"};
  char            com_filename[124];
  tm_topology_t   *topology;
  int             nb_processes = 0, nb_cores;
  int             i;
  tree_t          *comm_tree = NULL;
  int             *sigma, *k, *rr_sol;
  double          old_duration, new_duration, ratio;
  char            basename[100];
  affinity_mat_t  *aff_mat;

  TIC;
  topology = build_tm_topology(subtree_i, slurm_topology);
  nb_cores = nb_processing_units(topology);

  switch(matrix_type){
 case RND_MAT :
   sprintf(com_filename,"%s/%d.%s",COM_MAT_DIR,job->nb_procs,sparse[job->id % 3]);
   sprintf(basename,"%d.%s",job->nb_procs,sparse[job->id % 3]);
   nb_processes = build_aff_mat(com_filename, &aff_mat);
   break;
 case EXTRAPOLATED_MAT:
   sprintf(com_filename,"%s/%d.mat",COM_MAT_DIR,job->id % 8);
   sprintf(basename,"%d.mat",job->id % 8);
   nb_processes = build_aff_mat(com_filename, &aff_mat);
   nb_processes = upscale_mat(aff_mat, nb_processes, job->nb_procs);
   break;
 default:
   fprintf(stderr,"Unknow Matrix type %d\n",matrix_type);
   exit(-1);
}
  if (nb_processes > nb_cores) {
    fprintf(stderr, "Error: to many processes (%d)  for this topology (%d nodes)\n",
	    nb_processes, nb_cores);
    exit(-1);
  }

  set_constraints_from_job(topology,job,shift);

  comm_tree = build_tree_from_topology(topology, aff_mat, nb_processes, NULL, NULL);


   /* allocate the solution*/

   /*
     sigma[i] is such that  process i is mapped on core sigma[i]
     k[i] is such that core i exectutes process k_i

     size of sigma is the number of process (nb_processes)
     size of k is the number of cores/nodes (nb_cores)

     We must have numbe of process<=number of cores

     k[i] =-1 if no process is mapped on core i
   */

   sigma = (int *) MALLOC(sizeof(int) * nb_processes);
   k     = (int *) MALLOC(sizeof(int) * nb_cores);

   /* based on the build_tree_from_topology result, construct the perumtation and store it*/
   map_topology_simple(topology, comm_tree, sigma, nb_processes, k);


   printf("TopoMatch: ");
   new_duration = loc_eval_sol_inv(nb_processes, sigma, aff_mat, topology);

   rr_sol = (int*)MALLOC(sizeof(int)*nb_processes);
   for( i = 0 ; i < nb_processes ; i++ ){
    /*printf ("%d -> %d\n",i,i);*/
      rr_sol[i] = job->allocated_procs[i] - shift;
   }

   printf("RR: ");
   old_duration = loc_eval_sol_inv(nb_processes,rr_sol, aff_mat,  topology);

   double tm_duration = TOC *TM_FACTOR;

   if(new_duration == 0){
     ratio = 1;
   }else{
     ratio  = old_duration/new_duration;
   }

   printf("Job id: %d (%d procs)  Ratio = %f/%f = %f (%s)\n", job->id, job->nb_procs, old_duration, new_duration, ratio,basename);
   job->duration = (job->duration*communication_ratio/ratio) + (1-communication_ratio)*job->duration;

   printf("TM duration: %d\n",(int) tm_duration);

   job->duration += (int) tm_duration;

   FREE_affinity_mat(aff_mat);
   FREE_topology(topology);
   FREE(sigma);
   FREE(k);
   FREE(rr_sol);
   FREE_tree(comm_tree);
}


void check_freenodes(tm_topology_t *topo){
  int i, j, k;

  for(i=0 ; i<topo->nb_levels-1 ; i++){
    for(j=0 ; j<topo->nb_nodes[i] ; j++){
      int free_nodes = 0;
      for (k=0 ; k<topo->arity[i] ; k++){
	free_nodes += topo->free_nodes[i+1][j*topo->arity[i]+k];
	if(topo->free_nodes[i+1][j*topo->arity[i]+k] < 0){
  fprintf(stderr,"topo->free_nodes[%d][%d] = %d <0!\n", i+1,j*topo->arity[i]+k,topo->free_nodes[i+1][j*topo->arity[i]+k]);
	  exit(-1);
	}
      }

      if(topo->free_nodes[i][j] != free_nodes){
	fprintf(stderr,"topo->free_nodes[%d][%d]=%d != %d\n",i,j,
		topo->free_nodes[i][j], free_nodes);
	exit(-1);
      }
      /* if(i<=2) */
      /* 	printf("topo->free_nodes[%d][%d]=%d = %d",i,j,topo->free_nodes[i][j], free_nodes); */
    }
    /* if(i<=2) */
    /* 	printf("\n"); */
  }
  /* printf("xxxxx check corrrect\n"); */
}


job_queue_t *enqueue_job(job_queue_t *job_queue, job_t *job){
  job_queue_t *new_elem = CALLOC(1,sizeof(job_queue_t));

  /* printf("Enqueing job: ");display_job(job); */

  new_elem->job  = job;
  new_elem->next = NULL;

  if (!job_queue){
    job_queue = new_elem;
  } else {
    job_queue->last->next = new_elem;
  }

  job_queue->last = new_elem;

  return job_queue;
}



job_queue_t *read_swf_file(char* filename,   job_queue_t *job_queue){

  FILE *pf;
  char line[LINE_SIZE];
  int tab[N];
  int i;
  char *ptr= NULL;
  int line_n = 0;

  pf = fopen (filename,"r");
  if (!pf){
    fprintf(stderr,"Cannot open %s!\n",filename);
    exit(-1);
  }

  while(fgets(line, LINE_SIZE, pf)){

    char *l = line;


    line_n ++;
    i = 0;

    if (line[0] == ';')
      continue;

    while((ptr=strtok(l," \t"))){
      l = NULL;
      if((ptr[0] != '\n') && ( !isspace(ptr[0])) && (*ptr) && (ptr)){
	if(i <= N)
	  tab[i] = atoi(ptr);
	else{
	  fprintf(stderr, "More than %d entries in %s (line: %d)\n", N, filename, line_n);
	  exit(-1);
	}
	i++;
      }
    }

    int reduc = 1;
    if((tab[7]>=reduc))// && (tab[0] >= 506))
      job_queue = enqueue_job(job_queue, create_job(tab[0], tab[7]/reduc, tab[1]+global_date, tab[3]));}
  return job_queue;
}



event_list_t *create_event(enum event_type_t type, job_t *job, int date){
  event_list_t *event = MALLOC(sizeof(event_list_t));
  event -> type = type;
  event -> job  = job;
  event -> date = date;
  event -> next = NULL;

  return event;
}

event_list_t *add_event(event_list_t *event_list, event_list_t *event){

  /* printf ("Adding event : "); display_event(event); */

  if(!event_list){
    event_list = event;
  } else {
    event_list_t *iter=event_list, *prev = NULL;
    int done = 0;
    while (!done){
      /* if(iter) */
      /* 	printf("done= %d, event_date: %d, iter_date:%d\n",done, event->date,iter->date); */
      if (iter == NULL){
	prev -> next = event;
	done = 1;
      }else if(event->date < iter->date){
	event->next = iter;
	if(prev)
	  prev->next = event;
	else
	  event_list = event;
	done = 1;
      } else {
	prev = iter;
	iter = iter->next;
      }
    }
  }

  return event_list;
}


event_list_t *add_jobs_to_event_list(job_queue_t *job_queue, event_list_t *event_list){

  job_t *job;
  job_queue_t *prev_job_queue;

  while(job_queue){
    prev_job_queue = job_queue;
    job = job_queue->job;
    event_list  = add_event(event_list ,create_event(SUBMIT_JOB,job,job->submit_date));
    job_queue = job_queue->next;
    FREE(prev_job_queue);
  }

  return event_list;
}

void end_job(job_t *job, tm_topology_t *slurm_topology){
  int j ,ii, jj;

  printf ("Ending job: ");display_job(job);

  for(j = 0 ; j < job->nb_procs ; j++){
    jj = job->allocated_procs[j];
    for(ii = slurm_topology->nb_levels - 1 ; ii >= 0 ; ii --){
	jj = jj / slurm_topology->arity[ii];
	slurm_topology->free_nodes[ii][jj]++;
	/* printf("Increasing %d,%d\n",ii,jj); */
      }
  }

  /* int i; */
  /* for(i=0;i<slurm_topology->nb_levels; i++){ */
  /*   for(j=0;j<slurm_topology->nb_nodes[i];j++) */
  /*     printf("%d ", 	slurm_topology->free_nodes[i][j]); */
  /*   printf("\n"); */

  /* } */


}

void shift_one_level(int *subtree_i, int *subtree_j, tm_topology_t *slurm_topology, int nb_procs){
  int a;

  if(*subtree_i >0 ){
    if((*subtree_i) != slurm_topology->nb_levels - 1)
      slurm_topology->free_nodes[*subtree_i][*subtree_j] += nb_procs;
    (*subtree_i) --;
    a = slurm_topology->arity[*subtree_i];
    *subtree_j /= a;
  }

}


int find_best_subtree(int *subtree_i, int *subtree_j,
		      tm_topology_t *slurm_topology, int nb_procs, int test_subtree){
  int done = 0;
  int i,j;

  *subtree_i=0;
  *subtree_j=0;

  if (slurm_topology->free_nodes[0][0] < nb_procs ){
    printf("+++ Not enough resources\n");
    return 0;
  }
  slurm_topology->free_nodes[0][0] -= nb_procs;

  if((max_subtree_shift == 1)&&(test_subtree))
    return 1;

  /* find the subtree where nodes can be allocated */
  while(!done){
    i = *subtree_i+1;
    done = 1;
    for( j = (*subtree_j)*slurm_topology->arity[*subtree_i] ; j<((*subtree_j)+1)*slurm_topology->arity[*subtree_i] ; j++){
      /* fprintf(stderr,"slurm_topology->free_nodes[%d][%d] = %d\n",i,j,slurm_topology->free_nodes[i][j]); */
      if (slurm_topology->free_nodes[i][j] >= nb_procs ){
	done = 0;
	break;
      }
    }
    if(!done ){
      if(i != slurm_topology->nb_levels - 1){
	/* fprintf(stderr,"slurm_topology->free_nodes[%d][%d] = %d-%d\n",i,j,slurm_topology->free_nodes[i][j], job->nb_procs); */
	slurm_topology->free_nodes[i][j] -= nb_procs;
      }else{
	done = 1;
      }
      *subtree_i = i;
      *subtree_j = j;
      printf("subtree : %d,%d \n",i,j );
    }
  }
  return 1;
}



int schedule_job(job_t *job, tm_topology_t *slurm_topology){
  int subtree_i, subtree_j;
  int ii, jj, i, j;
  int sts;
  int proc_id;
  int nb_procs_to_allocate;

  printf("+++ Trying scheduling job: "); display_job(job);



  if(!find_best_subtree(&subtree_i, &subtree_j, slurm_topology, job->nb_procs,0))
      return -1;

  printf("subtree_i : %d , subtree_j : %d\n",subtree_i, subtree_j);

  sts = 1; /* number of nodes in the subtree*/
  for(i = subtree_i ; i < slurm_topology->nb_levels-1 ; i++)
    sts *= slurm_topology->arity[i];

  printf("sts: %d\n",sts);

  i                    = slurm_topology->nb_levels - 1;
  proc_id              = 0;
  nb_procs_to_allocate = job -> nb_procs;

  for (j = sts*subtree_j ; j < sts*(subtree_j+1) ; j++){
    /* printf("slurm_topology->free_nodes[%d][%d] == %d\n", i,j, slurm_topology->free_nodes[i][j]); */
    if ( slurm_topology->free_nodes[i][j] == 1 ){
      slurm_topology->free_nodes[i][j] = 0;
      job->allocated_procs[proc_id]    = j;
      proc_id ++;
      jj = j;
      /* printf("allocating : %d for job %d\n",j, job->id); */
      for(ii = i-1 ; ii > subtree_i ; ii--){
	jj = jj / slurm_topology->arity[ii];
	slurm_topology->free_nodes[ii][jj] --;
      }
      nb_procs_to_allocate --;
      if (nb_procs_to_allocate == 0)
	break;
    }
  }


  if((job->id>=0) && (job->id <=733000) && (simulation_type==1)){
    update_job_runtime(job, subtree_i, sts*subtree_j, slurm_topology);
  }

  if (nb_procs_to_allocate != 0){
    fprintf(stderr,"Error not enough free processor to allocate job\n");
    fprintf(stderr,"subtree_i : %d , subtree_j : %d\n",subtree_i, subtree_j);
    fprintf(stderr,"sts: %d\n",sts);
    fprintf(stderr,"nb_procs_to_allocate = %d\n",nb_procs_to_allocate);
    exit(-1);
  }


  /*  for(i=0;i<slurm_topology->nb_levels; i++){
    for(j=0;j<slurm_topology->nb_nodes[i];j++)
      printf("%d ", 	slurm_topology->free_nodes[i][j]);
    printf("\n");

  }
  */
  printf("+++ Success\n");

  return 0;
}

int tm_schedule_job(job_t *job, tm_topology_t *slurm_topology){
  int           subtree_i=0, subtree_j=0;
  int           ii, jj, i, j;
  int           sts;
  int           proc_id;
  int           nb_procs_to_allocate;
  tm_topology_t *topology;
  int           shift;
  int           last_level;
  int           nb_cores, nb_processes;
  char          com_filename[124];;
  char          *sparse[3]={"0.1","0.5","1"};
  tree_t        *comm_tree = NULL;
  int           *sigma, *k, *rr_sol;
  double        old_duration, new_duration, ratio;
  int           *constraints = NULL, nb_constraints;
  char          basename[100];
  affinity_mat_t *aff_mat;
  TIC;

  printf("+++ TM trying scheduling job: "); display_job(job);

  if(!find_best_subtree(&subtree_i, &subtree_j, slurm_topology, job->nb_procs,1)){
    TOC;
    return -1;
  }
  printf("subtree_i : %d , subtree_j : %d\n",subtree_i, subtree_j);

  for(i=0 ; i<subtree_level_shift ; i++){
    shift_one_level(&subtree_i, &subtree_j, slurm_topology, job->nb_procs);
    /* printf("-- subtree_i : %d , subtree_j : %d\n",subtree_i, subtree_j); */
  }

  printf("after shift subtree_i : %d , subtree_j : %d\n",subtree_i, subtree_j);

  sts = 1; /* number of nodes in the subtree*/
  for(i = subtree_i ; i < slurm_topology->nb_levels-1 ; i++)
    sts *= slurm_topology->arity[i];

  printf("sts: %d\n",sts);
  shift = sts*subtree_j;

  topology = build_tm_topology(subtree_i, slurm_topology);
  nb_cores = nb_processing_units(topology);
  switch(matrix_type){
  case RND_MAT :
  sprintf(com_filename,"%s/%d.%s", COM_MAT_DIR, job->nb_procs,sparse[job->id % 3]);
  sprintf(basename,"%d.%s", job->nb_procs,sparse[job->id % 3]);
  nb_processes = build_aff_mat(com_filename, &aff_mat);
  break;
  case EXTRAPOLATED_MAT:
    sprintf(com_filename,"%s/%d.mat", COM_MAT_DIR, job->id % 8);
    sprintf(basename,"%d.mat", job->id % 8);
    /* printf("%s\n\n",com_filename); */
    nb_processes = build_aff_mat(com_filename,&aff_mat);
    nb_processes = upscale_mat(aff_mat, nb_processes, job->nb_procs) ;
    break;
  default:
   fprintf(stderr,"Unknow Matrix type %d\n",matrix_type);
   exit(-1);
}


  if (nb_processes > nb_cores) {
    fprintf(stderr, "Error: to many processes (%d)  for this topology (%d nodes)\n",
	    nb_processes, nb_cores);
    exit(-1);
  }

  set_constraints_from_topology(topology,shift,subtree_j,sts,slurm_topology, &rr_sol, nb_processes);


  nb_constraints = check_constraints (topology, &constraints);
  TIC;
  comm_tree = kpartition_build_tree_from_topology(topology, aff_mat->mat, nb_processes, constraints, nb_constraints, NULL, NULL);
  float tm_dur = TOC;
  printf("TIMING: tm partitionning (%d) = %.3f\n",nb_processes, tm_dur); 

  FREE(constraints);


   /* allocate the solution*/

   /*
     sigma[i] is such that  process i is mapped on core sigma[i]
     k[i] is such that core i exectutes process k_i

     size of sigma is the number of process (nb_processes)
     size of k is the number of cores/nodes (nb_cores)

     We must have numbe of process<=number of cores

     k[i] =-1 if no process is mapped on core i
   */

   sigma = (int *) MALLOC(sizeof(int) * nb_processes);
   k     = (int *) MALLOC(sizeof(int) * nb_cores);

   /* based on the build_tree_from_topology result, construct the perumtation and store it*/
   map_topology_simple(topology, comm_tree, sigma, nb_processes, k);

   proc_id              = 0;
   nb_procs_to_allocate = job -> nb_procs;
  last_level = slurm_topology->nb_levels-1;


   for (i = 0 ; i < nb_processes ; i++){
     j = sigma[i]+shift;
     /* printf("%d ",sigma[i]); */
     slurm_topology->free_nodes[last_level][j] = 0;
      job->allocated_procs[proc_id]    = j;
      proc_id ++;
      jj = j;
      /* printf("allocating : %d for job %d\n",j, job->id); */
      for(ii = last_level-1 ; ii > subtree_i ; ii--){
	jj = jj / slurm_topology->arity[ii];
	slurm_topology->free_nodes[ii][jj] --;
      }
      nb_procs_to_allocate --;
    }

   printf("\n");
   if (nb_procs_to_allocate != 0){
     fprintf(stderr,"Error not enough free processor to allocate job\n");
     fprintf(stderr,"subtree_i : %d , subtree_j : %d\n",subtree_i, subtree_j);
     fprintf(stderr,"sts: %d\n",sts);
     fprintf(stderr,"nb_procs_to_allocate = %d\n",nb_procs_to_allocate);
     exit(-1);
   }

   printf("TopoMatch: ");
   new_duration = loc_eval_sol_inv(nb_processes, sigma, aff_mat, slurm_topology);

   printf("RR: ");
   old_duration = loc_eval_sol_inv(nb_processes,rr_sol, aff_mat, slurm_topology);


   if(new_duration == 0){
     ratio = 1;
   }else{
     ratio  = old_duration/new_duration;
   }

   printf("Job id: %d (%d procs)  Ratio = %f/%f = %f (%s)\n", job->id, job->nb_procs, old_duration, new_duration, ratio,basename);
   job->duration = (job->duration*communication_ratio/ratio) + (1-communication_ratio)*job->duration;


   FREE_affinity_mat(aff_mat);
   FREE_topology(topology);
   FREE(sigma);
   FREE(k);
   FREE(rr_sol);
   FREE_tree(comm_tree);
   double duration = TOC * TM_FACTOR;

   printf("+++ Success: %f -- %d\n",duration, (int) duration);

   return (int)duration;
}


job_queue_t *schedule_next_jobs(job_queue_t *job_queue, event_list_t **event_list, tm_topology_t *slurm_topology){
  int schedule_duration=0;
  job_queue_t *old_job_queue;
  job_t *job;
  if(job_queue){
    do{

      /* job with id > 7330000 are queued and running jobs*/
      if((job_queue->job->id < 733000) && (simulation_type == 2)){
	if(job_queue->job->id %2){
	  schedule_duration = tm_schedule_job(job_queue->job, slurm_topology);
	}else{
	  schedule_duration =  schedule_job(job_queue->job, slurm_topology);
	}
      }else{
	schedule_duration =  schedule_job(job_queue->job, slurm_topology);
      }
      if(schedule_duration >= 0){
	if(job_queue->job->id < 733000)
	  schedule_duration += SLURM_OVERHEAD;
	old_job_queue      = job_queue;
	job_queue          = job_queue->next;
	if(job_queue)
	  job_queue->last  = old_job_queue -> last;
	job                = old_job_queue->job;
	job->schedule_date = global_date;
	job->start_date    = global_date+schedule_duration;
	*event_list        = add_event(*event_list, create_event(START_JOB, job, job->start_date));
	FREE (old_job_queue);
      }
    }while(job_queue && (schedule_duration >= 0));
  }

    return job_queue;
}


event_list_t *process_next_event(event_list_t *event_list, job_queue_t **job_queue, tm_topology_t *slurm_topology){
  job_t *job;
  event_list_t *event;
  event       = event_list;
  job         = event->job;
  global_date = event->date;
  event_list  = event_list->next;
  printf("%d: Processing event:", global_date);display_event(event);
  switch (event->type){
  case SUBMIT_JOB:
    *job_queue = enqueue_job(*job_queue, job);
    break;
  case START_JOB:
    job->start_date = global_date;
    event_list = add_event(event_list,create_event(END_JOB,job,global_date + job->duration));
    break;
  case END_JOB:
    end_job(job, slurm_topology);
    job->end_date = global_date;
    if(job->id < 500){
      printf("+Done: ");display_job(job);
    } else {
      printf("Done: ");display_job(job);
    }
    FREE_job(job);
    break;
  default:
    fprintf(stderr,"Unknown event type: %d",event->type);
    exit(-1);
  }
  check_freenodes(slurm_topology);

  FREE(event);

  *job_queue = schedule_next_jobs(*job_queue, &event_list, slurm_topology);

  return event_list;

}





void empty_slurm_topology(tm_topology_t *slurm_topology){
  int i, j;

  slurm_topology->free_nodes = slurm_topology->node_id;

  i = slurm_topology->nb_levels-1;
  slurm_topology->free_nodes[i] = slurm_topology->node_id[i];
  /* each node at the bottom of teh topology is able to execute 1 process*/
  for(j=0 ; j<slurm_topology->nb_nodes[i] ; j++)
    slurm_topology->free_nodes[i][j]=1;

  for(i=slurm_topology->nb_levels-2; i>=0;i--){
    slurm_topology -> free_nodes[i] = slurm_topology->node_id[i];
    /* aggregate freenodes from the above level*/
    for(j=0;j<slurm_topology->nb_nodes[i];j++)
      slurm_topology->free_nodes[i][j] =
	slurm_topology->free_nodes[i+1][0]*slurm_topology->arity[i];
  }

  /* for(i=0;i<slurm_topology->nb_levels; i++){ */
  /*   for(j=0;j<slurm_topology->nb_nodes[i];j++) */
  /*     printf("%d ", 	slurm_topology->free_nodes[i][j]); */
  /*   printf("\n"); */

  /* } */

  slurm_topology->arity[slurm_topology->nb_levels -1] = 1;
}

int simulate_swf(int TGT_flag, char *arch_filename, char *swf_filename) {;
  tm_topology_t *slurm_topology;
  event_list_t *event_list = NULL;
  job_queue_t  *job_queue  = NULL, *running_job_queue  = NULL;
  char swf_running[1024];
  char swf_queued[1024];


   /* Parse the arch file according to its type XML or TGT*/
   if (TGT_flag == 1) {
     slurm_topology = tgt_to_tm(arch_filename);
   } else {
      slurm_topology = hwloc_to_tm(arch_filename);
   }
   empty_slurm_topology(slurm_topology);

   check_freenodes(slurm_topology);

   sprintf(swf_running,"%s.running",swf_filename);
   running_job_queue = read_swf_file(swf_running, NULL);
   running_job_queue = schedule_next_jobs(running_job_queue, &event_list, slurm_topology);
   printf("+++ running_job_queue = %p\n",running_job_queue);
   printf("+++ size= %d\n", slurm_topology->free_nodes[0][0]);


   sprintf(swf_queued,"%s.queued",swf_filename);
   job_queue = read_swf_file(swf_queued,NULL);
   job_queue = read_swf_file(swf_filename, job_queue);
   printf("+++ size= %d\n", slurm_topology->free_nodes[0][0]);
   printf("+++ job_queue = %p\n",job_queue);

   event_list = add_jobs_to_event_list(job_queue,event_list);
   job_queue = NULL;
   printf("+++ job_queue = %p\n",job_queue);
   printf("+++ size= %d\n", slurm_topology->free_nodes[0][0]);
   while(event_list)
     event_list = process_next_event (event_list, &job_queue, slurm_topology);
  printf("+++ size= %d\n", slurm_topology->free_nodes[0][0]);

   FREE_topology(slurm_topology);
   return 1;
}

void printUsage(char **argv) {
   fprintf(stderr,
	   "Usage: %s -t|x <Architecture file[tgt|xml]> -i <swf input file> -c <Communication ratio> -s <simulation type: 0 SLURM, 1: SLURM then TM 2: SLURM and TM>  -l <subtree_level_shift (default 0)> -k (mo subtree flag (oberwrite -l))) -m <matrix_type 1: Random mat (default) 2: Extrapolated mat> [-v verbose_level]\n",
	   argv[0]);
}

int main(int argc, char **argv) {
   char     c;
   char     *arch_filename = NULL;
   char     *swf_filename = NULL;
   int       TGT_flag = -1;
   int       verbose_level = ERROR;


   while ((c = getopt(argc, argv, "t:x:i:v:c:s:m:l:k")) != -1) {
      switch (c) {
	 case 't':
	    TGT_flag = 1;
	    arch_filename = optarg;
	    break;
	 case 'x':
	    TGT_flag = 0;
	    arch_filename = optarg;
	    break;
	 case 'i':
	    swf_filename = optarg;
	    break;
	 case 's':
	   simulation_type = atoi(optarg);
	   break;
	 case 'l':
	   subtree_level_shift = atoi(optarg);
	   if(subtree_level_shift < 0)
	     subtree_level_shift = 0;
	   break;
         case 'k':
	   max_subtree_shift = 1;
	   break;
	 case 'c':
	   communication_ratio = atof(optarg);
	    break;
         case 'v':
	   verbose_level  = atoi(optarg);
	   break;
         case 'm':
	   matrix_type  = atoi(optarg);
	   break;
	 case '?':
	    if (isprint(optopt))
	       fprintf(stderr, "Unknown option `-%c'.\n", optopt);
	    else
	       fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);

	    printUsage(argv);
	    abort();

	 default:
	    printUsage(argv);
	    abort();
      }
   }

   if(max_subtree_shift == 1)
     subtree_level_shift = 0;

   printf("Simulation_type: %d, communication_ratio: %f\n",
	  simulation_type, communication_ratio);

   if (!arch_filename || !swf_filename || (matrix_type != RND_MAT && matrix_type != EXTRAPOLATED_MAT)) {
      printUsage(argv);
      abort();
   }

   set_verbose_level(verbose_level);

   int res= simulate_swf(TGT_flag, arch_filename, swf_filename);

   printf("mem check!\n");
   MEM_CHECK();
   return res;

}
