#!/usr/bin/perl -w

use File::Basename;

$txt_filename = $ARGV[0];
$txt_filename =~ /\/(\d+)\/simulation/;

$hour = $1;
$max_time = $hour*3600; # in seconds

$dir     = dirname($txt_filename);

if (!-e $dir) {
   mkdir $dir;
   sleep 1;
   print STDERR "$dir created!\n";
}

$filename   = basename($txt_filename);
$res_filename = "$dir/$filename";
$res_filename =~ s/\.txt/\.res/;




# now we need to find the first dir where to read the input files. 
# this must be at least in the $hour directory but it can be found in other diredtory
# indeed, each directory 1, 2, 5, 10, ..., 100 is for the simulation of that amount of hours
# so if we need to extract information for 5 hours the information can be found in directory 5 but also in directory 10 or more. 


$dir = dirname($dir);
@dir_list = sort {$a <=> $b} map {basename $_ } (glob("$dir/?"),glob("$dir/??"),glob("$dir/???"));


my $ref_filename;
my $in_filename;


foreach $h (@dir_list) {
   next if ($h<$hour);
   $ref_filename = "$dir/$h/simulation_0_0.1.txt";
   $in_filename = "$dir/$h/$filename";
   last if (-e $in_filename);
}

print "$ref_filename\n";
print "$res_filename\n";

#if the file exists do not compute it again
if (-e $res_filename) {
   ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,
    $atime,$mtime1,$ctime,$blksize,$blocks)
     = stat($res_filename);
   ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,
    $atime,$mtime2,$ctime,$blksize,$blocks)
     = stat($ref_filename);

   if ($mtime1>$mtime2) {
      system "cat $res_filename";
      exit(-1);
   }
}


open IN,$ref_filename or print_default();
$dur_tab = ();
foreach $line (<IN>) {
   if ($line =~ /Done/) {
      ($id,$duration)=($line =~ /Done: id: (\d+) nb_procs: \d+ sub: \d+ sched: \d+ start: \d+ end: \d+ duration: (\d+)/);
      #print "$id\t$nb_procs\t$sub\t$sched\t$start\t$end\t$duration\n";
      #print "$id: $duration\n";
      if ($id < 733000) {
	 $dur_tab[$id] = $duration;
      }
   }
}
close IN;

open IN,$in_filename or print_default();


$max_stretch = 0;
$nb_jobs     = 0;
$makespan    = 0;
$sum_flow    = 0;
$max_flow    = 0;

foreach $line (<IN>) {
   if ($line =~ /Done/) {
      #print $line;
      ($id,$sub,$end,$duration)=($line =~ /Done: id: (\d+) nb_procs: \d+ sub: (\d+) sched: \d+ start: \d+ end: (\d+) duration: (\d+)/);
      next if ($id >= 733000);
      next if ($sub > $max_time);
      #print "$id\t$sub\t$end\t$duration\n";
      if ($makespan<$end) {
	 $makespan = $end;
      }
#      next if ($dur_tab[$id]>100);
      next if (!$duration);
      #print"id =$id\n";
      $stretch = ($end-$sub)/$dur_tab[$id];
      $sum_stretch +=$stretch;
      if ($max_stretch<$stretch) {
	 $max_stretch = $stretch;
      }
      $flow = ($end-$sub);
      $sum_flow +=$flow;
      if ($max_flow<$flow) {
	 $max_flow = $flow;
      }

      $nb_jobs++;


   }
}

open OUT,">$res_filename";

print OUT "Avg stretch: ".($sum_stretch)/$nb_jobs."\n";
print OUT "Max stretch: ".$max_stretch."\n";
print OUT "Avg flow: ".($sum_flow)/$nb_jobs."\n";
print OUT "Max flow: ".$max_flow."\n";
print OUT "Makespan: ".$makespan."\n";
close OUT;

print  "Avg stretch: ".($sum_stretch)/$nb_jobs."\n";
print  "Max stretch: ".$max_stretch."\n";
print  "Avg flow: ".($sum_flow)/$nb_jobs."\n";
print  "Max flow: ".$max_flow."\n";
print  "Makespan: ".$makespan."\n";


sub print_default{
   print  "Avg stretch: 0\n";
   print  "Max stretch: 0\n";
   print  "Avg flow: 0\n";
   print  "Max flow: 0\n";
   print  "Makespan: 0.\n";
   exit(-1);
}
