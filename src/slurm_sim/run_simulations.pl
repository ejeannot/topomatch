#!/usr/bin/perl -w

# call with ./run_simulations.pl

@M=("","riplay/random_matrices_res");

$prefix = "/Users/ejeannot/recherche/src/topomatch/trunk";
foreach $m (1){
   $outdir = "$prefix/src/slurm_sim/$M[$m]";
   foreach $d (1){
      foreach $s (0,2){
	 foreach $c (0.1,0.3,0.5){
	    $command= "/Users/ejeannot/recherche/src/topomatch/trunk/src/slurm_sim/slurm_sim -t $prefix/examples/topologies/curies.tgt -i $outdir/$d/$d.swf -c $c -s $s -m $m | tee ./mixed_$d\_$c\_".$s."_$m.txt";
	    print  "$command\n";
	    system "$command";
	    #sleep 60;
	 }
      }
   }
}

