set term pdf dashed enhanced color font ",9"
set output "makespan.pdf" 

set title 'Makespan'
set xlabel 'Percentage of Communication of the Application Runtime' 
show xlabel
set ylabel 'Ratio to SLURM'
show ylabel

set key bottom right
#set xtics 0 20000
#set yrange [-100:1200]

plot "makespan.res" using  ($1*100):($2/$3)   with linespoint lw 3 title "SLURM then TM" ,\
     "makespan.res" using  ($1*100):($2/$4)   with linespoint lw 3 title "SLURM and TM" 