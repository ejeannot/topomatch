#!/usr/bin/perl -w

use File::Basename;

$txt_filename = $ARGV[0];


$dir         = dirname($txt_filename);
$dur_tab = ();


open IN,"$dir/simulation_0_0.1.txt" or print_default();
foreach $line (<IN>) {
   if ($line =~ /Done/) {
      ($id,$duration)=($line =~ /Done: id: (\d+) nb_procs: \d+ sub: \d+ sched: \d+ start: \d+ end: \d+ duration: (\d+)/);
      # print "$id\t$nb_procs\t$sub\t$sched\t$start\t$end\t$duration\n";
      if ($id < 733000) {
	 $dur_tab[$id] = $duration;
      }
   }
}
close IN;

open IN,$ARGV[0] or print_default();


$max_stretch = 0;
$nb_jobs     = 0;
$makespan    = 0;
$sum_flow    = 0;
$max_flow    = 0;

foreach $line (<IN>) {
   if ($line =~ /Done/) {
      #print $line;
      ($id,$sub,$end,$duration)=($line =~ /Done: id: (\d+) nb_procs: \d+ sub: (\d+) sched: \d+ start: \d+ end: (\d+) duration: (\d+)/);
      next if ($id >= 733000);
      if ($makespan<$end) {
	 $makespan = $end;
      }
#      next if ($dur_tab[$id]>100);
      next if (!$duration);
      $stretch = ($end-$sub)/$dur_tab[$id];
      print "$id\t$sub\t$end\t$duration\t: $stretch\n";
      $sum_stretch +=$stretch;
      if ($max_stretch<$stretch) {
	 $max_stretch = $stretch;
      }
      $flow = ($end-$sub);
      $sum_flow +=$flow;
      if ($max_flow<$flow) {
	 $max_flow = $flow;
      }

      $nb_jobs++;


   }
}

print  "Avg stretch: ".($sum_stretch)/$nb_jobs."\n";
print  "Max stretch: ".$max_stretch."\n";
print  "Avg flow: ".($sum_flow)/$nb_jobs."\n";
print  "Max flow: ".$max_flow."\n";
print  "Makespan: ".$makespan."\n";


sub print_default{
   print  "Avg stretch: 0\n";
   print  "Max stretch: 0\n";
   print  "Avg flow: 0\n";
   print  "Max flow: 0\n";
   print  "Makespan: 0.\n";
   exit(-1);
}
