#!/usr/bin/perl

$infile = $ARGV[0];
$outfile= $infile;
$outfile=~s/.txt/.csv/;

open IN,$infile;
open OUT , ">$outfile";

%nb=();
%mr=();

foreach $line (<IN>) {
   if ($line =~ /Job id: (\d+) \((\d+) procs\)  Ratio = \S+ = (\S+) \((\S+)\)/){
      $id = $1;
      $n = $2;
      $r = $3;
      $f = $4;
      $nb{$f}++;
      $mr{$f}+=$r;
      $r =~ s/\./,/;
      print OUT "$id\t$n\t$r\t$f\n";
   }
}
close IN;
close OUT;

my @keys = sort { $mr{$a}/$nb{$a} <=> $mr{$b}/$nb{$b} } keys(%nb);

foreach $k (@keys) {
   printf "$k: %.3f\n", $mr{$k}/$nb{$k};
}
