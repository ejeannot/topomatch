#!/usr/bin/perl -w

#call with generate_all_res.pl ./riplay/random_matrices_res

%metrics = ("Avg stretch" => "avg_stretch",
	    "Max stretch" => "max_stretch",
	    "Avg flow"    => "avg_flow",
	    "Max flow"    => "max_flow",
	    "Makespan"    => "makespan");

$prefix="/Users/ejeannot/recherche/src/topomatch/trunk/src/slurm_sim/$ARGV[0]";

@files = glob ("*.plot");

@durations = (1,2,3,4,5,10,20,50,100);
#@durations = (1..20);
#@durations = (100);
foreach $d (@durations){
   printf "processing $prefix/$d...\n"; 
   if (!-e "$prefix/$d") {
      mkdir "$prefix/$d";
      print STDERR "$prefix/$d  created!\n";
   }
   foreach $k (keys %metrics) {
      $cmd = "./generate_res_hour.pl $prefix $d \"$k\" > $prefix/$d/$metrics{$k}.res";
      printf "$cmd\n";
      `$cmd`;
   }
   foreach $file (@files){
      move_file ($d, $file,"$prefix/$d");
   }
   system "cd $prefix/$d;gnuplot *plot";
}



$duration = join",",@durations;
foreach $s (0.1,0.3,0.5){
   printf "processing for sparsity $s\n"; 
   foreach $k (keys %metrics) {
      printf "./generate_res_sparsity.pl $prefix $s $duration \"$k\" > $prefix/$s/$metrics{$k}.res\n";
      `./generate_res_sparsity.pl $prefix $s $duration \"$k\" > $prefix/$s/$metrics{$k}.res`;
   }
   foreach $file (@files){
      move_file2 ($s, $file,"$prefix/$s");
   }
   system "cd $prefix/$s;gnuplot *plot";
}



sub move_file{
   my ($d,$infile,$dest) = @_;
   $outfile = "$dest/$infile";
   open IN,$infile;
   open OUT,">$outfile";
   foreach $line (<IN>) {
      if ($line =~ /set title/) {
	 if ($d ==1){
	    $line = "set title \'$d hour simulation\'\n";
	 } else {
	    $line = "set title \'$d hours simulation\'\n";
	 }

      }
      print OUT $line;
   }
   close IN;
   close OUT;
}

sub move_file2{
   my ($s,$infile,$dest) = @_;
   $outfile = "$dest/$infile";
   open IN,$infile;
   open OUT,">$outfile";
   foreach $line (<IN>) {
      if ($line =~ /set title/) {
	 $line = sprintf "set title \'Percentage of communication: %d\'\n",int($s*100);
      } elsif ($line =~ /set\s+xlabel/) {
	 $line = "set xlabel \'Number of simulated hours\'\n";
      }
      $line =~ s/\(\$1\*100\)/1/;
      print OUT $line;
   }
   close IN;
   close OUT;
}
