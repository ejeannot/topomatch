#!/usr/bin/perl -w

use File::Basename;

$dir         = dirname($ARGV[0]);

$dur_tab = ();

$file_out = 




open IN,$ARGV[0];




$max_stretch = 0;
$nb_jobs     = 0;
$makespan    = 0;
$sum_flow    = 0;
$max_flow    = 0;

foreach $line (<IN>) {
   if ($line =~ /Done/) {
      #print $line;
      ($id,$sub,$end,$duration)=($line =~ /Done: id: (\d+) nb_procs: \d+ sub: (\d+) sched: \d+ start: \d+ end: (\d+) duration: (\d+)/);
      next if ($id >= 733000);
      #print "$id\t$sub\t$end\t$duration\n";
      if ($makespan<$end) {
	 $makespan = $end;
      }
#      next if ($dur_tab[$id]>100);
      next if (!$duration);
      $stretch = ($end-$sub)/$dur_tab[$id];
      $sum_stretch +=$stretch;
      if ($max_stretch<$stretch) {
	 $max_stretch = $stretch;
      }
      $flow = ($end-$sub);
      $sum_flow +=$flow;
      if ($max_flow<$flow) {
	 $max_flow = $flow;
      }

      $nb_jobs++;


   }
}

printf"Avg stretch: ".($sum_stretch)/$nb_jobs."\n";;
printf"Max stretch: ".$max_stretch."\n";;
printf"Avg flow: ".($sum_flow)/$nb_jobs."\n";;
printf"Max flow: ".$max_flow."\n";;
printf"Makespan: ".$makespan."\n";;
